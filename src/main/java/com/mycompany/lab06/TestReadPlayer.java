package com.mycompany.lab06;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestReadPlayer {

    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream fis = null;
        try {
            File file = new File("Players.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Player x = (Player) ois.readObject();
            Player o = (Player) ois.readObject();
            ois.close();
            fis.close();
            System.out.println(o);
            System.out.println(x);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(TestReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(TestReadFriend.class.getName()).log(Level.SEVERE, null, ex);  
            }
        }
    }
}
